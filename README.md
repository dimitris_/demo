# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A demo application with spring boot and mvc.
* Version 0.0.1-SNAPSHOT

### How do I get set up? ###

* Run schema-postgresql.sql to restore the schema.
* Run v1_data_role to insert roles in the database.
