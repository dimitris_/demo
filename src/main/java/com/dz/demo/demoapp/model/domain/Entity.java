package com.dz.demo.demoapp.model.domain;

/**
 * The type Entity.
 */
public class Entity {

    /** The id. */
    private Long id;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {  return id; }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) { this.id = id; }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                '}';
    }
}
