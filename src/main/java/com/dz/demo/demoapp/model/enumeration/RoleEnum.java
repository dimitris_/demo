package com.dz.demo.demoapp.model.enumeration;

public enum RoleEnum {

    STANDARD, ADMIN, NA;

    public static RoleEnum getByName(final String name) {
        for (final RoleEnum role : RoleEnum.values()) {
            if (role.name().equals(name)) {
                return role;
            }
        }
        return NA;
    }
}
