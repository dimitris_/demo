package com.dz.demo.demoapp.model.transfer;

/**
 * The type Ip dto.
 */
public class IPDto {

    /** The id. */
    private Long id;

    /** The ip. */
    private String ip;

    /** The hostname. */
    private String hostname;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() { return id; }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) { this.id = id; }

    /**
     * Gets ip.
     *
     * @return the ip
     */
    public String getIp() { return ip; }

    /**
     * Sets ip.
     *
     * @param ip the ip
     */
    public void setIp(String ip) { this.ip = ip; }

    /**
     * Gets hostname.
     *
     * @return the hostname
     */
    public String getHostname() { return hostname; }

    /**
     * Sets hostname.
     *
     * @param hostname the hostname
     */
    public void setHostname(String hostname) { this.hostname = hostname; }

    @Override
    public String toString() {
        return "IPDto{" +
                "ip='" + ip + '\'' +
                ", hostname='" + hostname + '\'' +
                '}';
    }
}
