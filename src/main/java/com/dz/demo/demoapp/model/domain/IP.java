package com.dz.demo.demoapp.model.domain;

/**
 * The IP domain object .
 */
public class IP extends Entity {

    /** The ip. */
    private String ip;

    /** The hostname. */
    private String hostname;

    /**
     * Gets ip.
     *
     * @return the ip
     */
    public String getIp() { return ip; }

    /**
     * Sets ip.
     *
     * @param ip the ip
     */
    public void setIp(String ip) { this.ip = ip; }

    /**
     * Gets hostname.
     *
     * @return the hostname
     */
    public String getHostname() { return hostname; }

    /**
     * Sets hostname.
     *
     * @param hostname the hostname
     */
    public void setHostname(String hostname) { this.hostname = hostname; }

    @Override
    public String toString() {
        return "IP{" +
                "ip='" + ip + '\'' +
                ", hostname='" + hostname + '\'' +
                '}';
    }
}
