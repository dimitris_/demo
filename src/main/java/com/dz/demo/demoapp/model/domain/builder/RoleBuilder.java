package com.dz.demo.demoapp.model.domain.builder;

import com.dz.demo.demoapp.model.domain.Role;
import com.dz.demo.demoapp.model.enumeration.RoleEnum;

/**
 * The type Role builder.
 */
public class RoleBuilder {

    private Role role;

    /**
     * Instantiates a new Role builder.
     */
    public RoleBuilder() {
        this.role = new Role();
    }

    /**
     * With user id role builder.
     *
     * @param userId the user id
     * @return the role builder
     */
    public RoleBuilder withUserId(Long userId) {
        this.role.setUserId(userId);
        return this;
    }

    /**
     * With role role builder.
     *
     * @param role the role
     * @return the role builder
     */
    public RoleBuilder withRole(RoleEnum role) {
        this.role.setRole(role);
        return this;
    }

    /**
     * Build role.
     *
     * @return the role
     */
    public Role build() {
        return this.role;
    }
}
