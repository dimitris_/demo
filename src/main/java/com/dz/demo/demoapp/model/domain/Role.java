package com.dz.demo.demoapp.model.domain;

import com.dz.demo.demoapp.model.enumeration.RoleEnum;

/**
 * The type Role.
 */
public class Role extends Entity{

    /** The user id. */
    private Long userId;

    /** The role enum. */
    private RoleEnum role;

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public RoleEnum getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(RoleEnum role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Role{" +
                "userId=" + userId +
                ", role=" + role +
                '}';
    }
}
