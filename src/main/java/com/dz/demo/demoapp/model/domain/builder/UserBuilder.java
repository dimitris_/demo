package com.dz.demo.demoapp.model.domain.builder;

import com.dz.demo.demoapp.model.domain.User;
import com.dz.demo.demoapp.model.enumeration.RoleEnum;

public class UserBuilder {

    private User user;

    public UserBuilder() {
        this.user = new User();
    }

    public UserBuilder withId(Long id) {
        this.user.setId(id);
        return this;
    }

    public UserBuilder withName(String name) {
        this.user.setName(name);
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.user.setPassword(password);
        return this;
    }

    public UserBuilder withEmail(String email) {
        this.user.setEmail(email);
        return this;
    }

    public UserBuilder withRole(RoleEnum role) {
        this.user.setRole(role);
        return this;
    }

    public User build() {
        return this.user;
    }
}
