package com.dz.demo.demoapp.model.domain;

import com.dz.demo.demoapp.model.enumeration.RoleEnum;

/**
 * The User domain object.
 */
public class User extends Entity {

    /** The user's name */
    private String name;

    /** The user's password */
    private String password;

    /** The user's email */
    private String email;

    /** The user role. */
    private RoleEnum role;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() { return name; }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) { this.name = name; }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() { return password; }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) { this.password = password; }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() { return email; }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) { this.email = email; }

    /**
     * Gets role.
     *
     * @return the role
     */
    public RoleEnum getRoleEnum() { return role; }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(RoleEnum role) { this.role = role; }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
