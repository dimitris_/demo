package com.dz.demo.demoapp.model.domain.builder;

import com.dz.demo.demoapp.model.domain.IP;

public class IPBuilder {

    private IP ip;

    public IPBuilder() {
        this.ip = new IP();
    }

    public IPBuilder withId(Long id) {
        this.ip.setId(id);
        return this;
    }

    public IPBuilder withIP(String ip) {
        this.ip.setIp(ip);
        return this;
    }

    public IPBuilder withHostname(String hostname) {
        this.ip.setHostname(hostname);
        return this;
    }

    public IP build() {
        return this.ip;
    }
}
