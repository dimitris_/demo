package com.dz.demo.demoapp.model.transfer;


import com.dz.demo.demoapp.model.enumeration.RoleEnum;

/**
 * The type User dto.
 */
public class UserDto {

    /** The user id. */
    private Long id;

    /** The user name. */
    private String name;

    /** The user password. */
    private String password;

    /** The user email. */
    private String email;

    /** The user roleEnum. */
    private String role;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() { return id; }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) { this.id = id; }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() { return name; }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) { this.name = name; }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() { return password; }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) { this.password = password; }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() { return email; }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) { this.email = email; }

    /**
     * Gets role.
     *
     * @return the role
     */
    public String getRole() { return role; }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(String role) { this.role = role; }

    @Override
    public String toString() {
        return "UserDto{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
