package com.dz.demo.demoapp.service;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.model.domain.Role;

/**
 * The interface Role management service.
 */
public interface RoleManagementService {

    /**
     * Assign.
     *
     * @param role the role
     * @throws DemoException     the demo exception
     * @throws DemoDataException the demo data exception
     */
    void assign(Role role) throws DemoException, DemoDataException;

    /**
     * Remove mapping.
     *
     * @param userId the user id
     * @throws DemoException     the demo exception
     * @throws DemoDataException the demo data exception
     */
    void removeMapping(Long userId) throws DemoException, DemoDataException;
}
