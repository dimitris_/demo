package com.dz.demo.demoapp.service;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.model.domain.Role;
import com.dz.demo.demoapp.model.domain.builder.RoleBuilder;
import com.dz.demo.demoapp.repository.RoleManagementRepository;
import com.dz.demo.demoapp.repository.UserRepository;
import com.dz.demo.demoapp.model.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleManagementService roleManagementService;

    /** {@inheritDoc} */
    @Override
    public void create(final User user) throws DemoException, DemoDataException {

        userRepository.create(user);
        Role role = new RoleBuilder()
                .withUserId(user.getId())
                .withRole(user.getRoleEnum())
                .build();
        roleManagementService.assign(role);
    }

    /** {@inheritDoc} */
    @Override
    public void delete(final Long id) throws DemoException, DemoDataException {

        roleManagementService.removeMapping(id);
        // TODO: In case of exception on user deletion, rollback removeMapping()
        userRepository.delete(id);

    }

    @Override
    public List<User> getUsers() throws DemoException {

        return userRepository.getUsers();
    }

    @Override
    public void update(final User user) throws DemoException, DemoDataException {

        userRepository.update(user);

    }
}
