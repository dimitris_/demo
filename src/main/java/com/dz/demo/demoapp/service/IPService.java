package com.dz.demo.demoapp.service;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.model.domain.IP;

import java.util.List;

/**
 * The interface Ip service.
 */
public interface IPService {

    /**
     * Create ip object.
     *
     * @param ip the ip
     * @throws DemoException the demo exception
     */
    void createIP(IP ip) throws DemoException;

    /**
     * Gets i ps.
     *
     * @return the IPs
     * @throws DemoException the demo exception
     */
    List<IP> getIPs() throws DemoException;

    /**
     * Update ip.
     *
     * @param ip the ip
     * @throws DemoException     the demo exception
     * @throws DemoDataException the demo data exception
     */
    void updateIP(IP ip) throws DemoException, DemoDataException;

    /**
     * Delete ip.
     *
     * @param id the id
     * @throws DemoException     the demo exception
     * @throws DemoDataException the demo data exception
     */
    void deleteIP(long id) throws DemoException, DemoDataException ;
}
