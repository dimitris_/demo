package com.dz.demo.demoapp.service;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.repository.IPRepository;
import com.dz.demo.demoapp.model.domain.IP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IPServiceImpl implements IPService {

    @Autowired
    private IPRepository ipRepository;

    /** {@inheritDoc} */
    @Override
    public void createIP(final IP ip) throws DemoException {

        ipRepository.createIP(ip);
    }

    /** {@inheritDoc} */
    @Override
    public List<IP> getIPs() throws DemoException {

        return ipRepository.getIPs();
    }

    /** {@inheritDoc} */
    @Override
    public void updateIP(final IP ip) throws DemoException, DemoDataException {

        ipRepository.updateIP(ip);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteIP(final long id) throws DemoException, DemoDataException  {

        ipRepository.deleteIP(id);

    }
}
