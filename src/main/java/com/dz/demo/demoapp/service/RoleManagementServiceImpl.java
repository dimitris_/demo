package com.dz.demo.demoapp.service;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.model.domain.Role;
import com.dz.demo.demoapp.repository.RoleManagementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleManagementServiceImpl implements RoleManagementService {

    @Autowired
    private RoleManagementRepository roleManagementRepository;

    @Override
    public void assign(Role role) throws DemoException, DemoDataException {

        roleManagementRepository.assign(role);
    }

    @Override
    public void removeMapping(Long id) throws DemoException, DemoDataException {

        roleManagementRepository.removeMapping(id);
    }
}
