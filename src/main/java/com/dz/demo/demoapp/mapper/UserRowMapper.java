package com.dz.demo.demoapp.mapper;

import com.dz.demo.demoapp.model.domain.User;
import com.dz.demo.demoapp.model.enumeration.RoleEnum;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {


    @Nullable
    @Override
    public User mapRow(final ResultSet resultSet, final int i) throws SQLException {

        final User us = new User();

        us.setId(resultSet.getLong("ID"));
        us.setName(resultSet.getString("NAME"));
        us.setEmail(resultSet.getString("EMAIL"));
        us.setRole(RoleEnum.getByName(resultSet.getString("ROLE")));

        return us;
    }
}
