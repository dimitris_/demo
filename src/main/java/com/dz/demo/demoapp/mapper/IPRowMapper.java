package com.dz.demo.demoapp.mapper;

import com.dz.demo.demoapp.model.domain.IP;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IPRowMapper implements RowMapper<IP> {

    @Nullable
    @Override
    public IP mapRow(final ResultSet resultSet, final int i) throws SQLException {

        final IP ip = new IP();

        ip.setIp(resultSet.getString("IP"));
        ip.setHostname(resultSet.getString("HOSTNAME"));
        ip.setId(resultSet.getLong("ID"));

        return ip;
    }

}
