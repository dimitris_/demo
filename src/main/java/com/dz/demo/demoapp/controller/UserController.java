package com.dz.demo.demoapp.controller;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.model.enumeration.RoleEnum;
import com.dz.demo.demoapp.service.UserService;
import com.dz.demo.demoapp.model.domain.User;
import com.dz.demo.demoapp.model.domain.builder.UserBuilder;
import com.dz.demo.demoapp.model.transfer.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public User create(@RequestBody UserDto userDto) throws DemoException, DemoDataException {

        User user = new UserBuilder().withName(userDto.getName())
                .withPassword(userDto.getPassword())
                .withEmail(userDto.getEmail())
                .withRole(RoleEnum.getByName(userDto.getRole()))
                .build();
        userService.create(user);


        return user;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public User delete(@RequestBody final User user) throws DemoException, DemoDataException {

        userService.delete(user.getId());

        return user;
    }

    @ResponseBody
    @RequestMapping(value = "/fetch", method = RequestMethod.GET)
    public List<User> getUsers() throws DemoException, DemoDataException {

        return userService.getUsers();

    }

    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public User update(@RequestBody final User user) throws DemoException, DemoDataException {

        userService.update(user);

        return user;
    }


}
