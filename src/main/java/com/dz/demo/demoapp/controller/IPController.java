package com.dz.demo.demoapp.controller;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.service.IPService;
import com.dz.demo.demoapp.model.domain.IP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/ip")
public class IPController {

    @Autowired
    private IPService ipService;

    @ResponseBody
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public IP createIP(@RequestBody IP ip) throws DemoException {

        ipService.createIP(ip);
        return ip;

    }

    @ResponseBody
    @RequestMapping(value = "/fetch", method = RequestMethod.GET)
    public List<IP> getIPs() throws DemoException {

        return ipService.getIPs();
    }

    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public IP updateIP(@RequestBody IP ip) throws DemoException, DemoDataException  {

        ipService.updateIP(ip);
        return ip;
    }

    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public IP deleteIP(@RequestBody IP ip) throws DemoException, DemoDataException {

        ipService.deleteIP(ip.getId());
        return ip;
    }

}
