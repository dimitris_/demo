package com.dz.demo.demoapp.exception;

/**
 * The type Demo exception.
 */
public class DemoException extends Exception {

    /**
     * Instantiates a new Demo exception.
     */
    public DemoException() {}

    /**
     * Instantiates a new Demo exception.
     *
     * @param message the message
     */
    public DemoException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Demo exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public DemoException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
