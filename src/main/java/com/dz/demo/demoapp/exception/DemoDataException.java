package com.dz.demo.demoapp.exception;

/**
 * The type Demo data exception.
 */
public class DemoDataException extends Exception {

    /**
     * Instantiates a new Demo data exception.
     */
    public DemoDataException() {}


    /**
     * Instantiates a new Demo data exception.
     *
     * @param message the message
     */
    public DemoDataException(String message) {
        super(message);
    }

}
