package com.dz.demo.demoapp.repository;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.model.domain.Role;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.text.MessageFormat;

@Repository
public class RoleManagementRepositoryImpl extends BaseRepository implements RoleManagementRepository {

    @Override
    public void assign(final Role role) throws DemoException, DemoDataException {

        int rowsAffected;
        try {
            final KeyHolder keyHolder = new GeneratedKeyHolder();

            rowsAffected = demoJdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps =
                                connection.prepareStatement(getSqlCommands("ASSIGN_ROLE"), new String[] {"ID"});
                        ps.setLong(1, role.getUserId());
                        ps.setString(2, role.getRole().name());
                        return ps;
                    },
                    keyHolder);

            if (rowsAffected != 1) {
                final String errorMessage = MessageFormat.format("Error while assign user role. [role={0}, rowsAffected={1}]", role, rowsAffected);
                throw new DemoDataException(errorMessage);
            }

        } catch (Exception e) {
            final String errorMessage = MessageFormat.format("Error on assign user role [role={0}, Error=[{1}]", role, e.getMessage());
            throw new DemoException(errorMessage, e);
        }
    }

    @Override
    public void removeMapping(final Long userId) throws DemoException, DemoDataException {

        int rowsAffected;
        try {
            rowsAffected = demoJdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps =
                                connection.prepareStatement(getSqlCommands("REMOVE_MAPPING"), new String[] {"ID"});
                        ps.setLong(1, userId);
                        return ps;
                    });
            if (rowsAffected != 1) {
                final String errorMessage = MessageFormat.format("Error while removing user role map. [UserId={0}, rowsAffected={1}]", userId, rowsAffected);
                throw new DemoDataException(errorMessage);
            }

        } catch (Exception e) {
            final String errorMessage = MessageFormat.format("Error on removeMapping [userId={0}, Error=[{1}]", userId, e.getMessage());
            throw new DemoException(errorMessage, e);
        }
    }
}
