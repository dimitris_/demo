package com.dz.demo.demoapp.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ResourceBundle;

public class BaseRepository {

    @Autowired
    protected JdbcTemplate demoJdbcTemplate;

    /** The SQL_COMMANDS parameter. */
    private final ResourceBundle SQL_COMMANDS = ResourceBundle.getBundle("sql/demo_sql");

    protected String getSqlCommands(final String key) {
        if (SQL_COMMANDS.containsKey(key)) {
            return SQL_COMMANDS.getString(key);
        }
        return null;
    }
}
