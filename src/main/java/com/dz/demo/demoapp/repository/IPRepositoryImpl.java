package com.dz.demo.demoapp.repository;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.mapper.IPRowMapper;
import com.dz.demo.demoapp.model.domain.IP;

import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.text.MessageFormat;
import java.util.List;

@Repository
public class IPRepositoryImpl extends BaseRepository implements IPRepository {

    /** {@inheritDoc} */
    @Override
    public void createIP(IP ip) throws DemoException {

        try {
            final KeyHolder keyHolder = new GeneratedKeyHolder();
            demoJdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps =
                                connection.prepareStatement(getSqlCommands("CREATE_IP"), new String[] {"ID"});
                        ps.setString(1, ip.getIp());
                        ps.setString(2, ip.getHostname());
                        return ps;
                    }, keyHolder);

            final Long id = keyHolder.getKey().longValue();
            ip.setId(id);
        } catch (Exception e) {
            final String errorMessage = MessageFormat.format("Error on createIP [ip={0}, Error=[{1}]", ip, e.getMessage());
            throw new DemoException(errorMessage, e);
        }
    }

    /** {@inheritDoc} */
    @Override
    public List<IP> getIPs() throws DemoException {

        List<IP> ipList;
        try {
           ipList = demoJdbcTemplate.query(getSqlCommands("GET_IPs"), new IPRowMapper());
        }
        catch (Exception e){
            final String errorMessage = MessageFormat.format("Error on getIPs [Error=[{0}]", e.getMessage());
            throw new DemoException(errorMessage, e);
        }

        return ipList;
    }

    /** {@inheritDoc} */
    @Override
    public void updateIP(IP ip) throws DemoDataException, DemoException {

        int rowsAffected;
        try {
            rowsAffected = demoJdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps =
                                connection.prepareStatement(getSqlCommands("UPDATE_IP"), new String[] {"ID"});
                        ps.setString(1, ip.getIp());
                        ps.setString(2, ip.getHostname());
                        ps.setLong(3, ip.getId());
                        return ps;
                    });

            if (rowsAffected != 1) {
                final String errorMessage = MessageFormat.format("Error while updating IP. [IP={0}, rowsAffected={1}]", ip.getIp(), rowsAffected);
                throw new DemoDataException(errorMessage);
            }

        } catch (Exception e) {
            final String errorMessage = MessageFormat.format("Error on updateIP [ip={0}, Error=[{1}]", ip, e.getMessage());
            throw new DemoException(errorMessage, e);
        }

    }


    /** {@inheritDoc} */
    @Override
    public void deleteIP(long id) throws DemoDataException, DemoException {

        int rowsAffected;
        try {
            rowsAffected = demoJdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps =
                                connection.prepareStatement(getSqlCommands("DELETE_IP"), new String[] {"ID"});
                        ps.setLong(1, id);
                        return ps;
                    });

            if (rowsAffected != 1) {
                final String errorMessage = MessageFormat.format("Error while removing IP. [ID={0}, rowsAffected={1}]", id, rowsAffected);
                throw new DemoDataException(errorMessage);
            }

        } catch (Exception e) {
            final String errorMessage = MessageFormat.format("Error on deleteIP [id={0}, Error=[{1}]", id, e.getMessage());
            throw new DemoException(errorMessage, e);
        }
    }
}
