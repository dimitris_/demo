package com.dz.demo.demoapp.repository;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.mapper.UserRowMapper;
import com.dz.demo.demoapp.model.domain.User;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.text.MessageFormat;
import java.util.List;

@Repository
public class UserRepositoryImpl extends BaseRepository implements UserRepository {

    /** {@inheritDoc} */
    @Override
    public void create(User user) throws DemoException {

        try {

            final KeyHolder keyHolder = new GeneratedKeyHolder();

            demoJdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps =
                                connection.prepareStatement(getSqlCommands("CREATE_USER"), new String[] {"ID"});
                        ps.setString(1, user.getName());
                        ps.setString(2, user.getPassword());
                        ps.setString(3, user.getEmail());
                        return ps;
                    },
                    keyHolder);

            final Long id = keyHolder.getKey().longValue();
            user.setId(id);

        } catch (Exception e) {
            final String errorMessage = MessageFormat.format("Error while creating user [user={0}, Error=[{1}]", user, e.getMessage());
            throw new DemoException(errorMessage, e);
        }

    }

    /** {@inheritDoc} */
    @Override
    public void delete(Long id) throws DemoException, DemoDataException {

        int rowsAffected;
        try {
            rowsAffected = demoJdbcTemplate.update(
                connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(getSqlCommands("DELETE_USER"));
                    ps.setLong(1, id);
                    return ps;
                }
            );

            if (rowsAffected != 1) {
                final String errorMessage = MessageFormat.format("Error while removing user. [ID={0}, rowsAffected={1}]", id, rowsAffected);
                throw new DemoDataException(errorMessage);
            }

        } catch (Exception e) {
            final String errorMessage = MessageFormat.format("Error on delete user [id={0}, Error=[{1}]", id, e.getMessage());
            throw new DemoException(errorMessage, e);
        }
    }

    @Override
    public List<User> getUsers() throws DemoException {

        List<User> userList;
        try {
            userList = demoJdbcTemplate.query(getSqlCommands("GET_USERS"), new UserRowMapper());
        }
        catch (Exception e){
            final String errorMessage = MessageFormat.format("Error on getUsers [Error=[{0}]", e.getMessage());
            throw new DemoException(errorMessage, e);
        }

        return userList;
    }

    @Override
    public void update(final User user) throws DemoException, DemoDataException {
        int rowsAffected;
        try {
            rowsAffected = demoJdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps =
                                connection.prepareStatement(getSqlCommands("UPDATE_USER"));
                        ps.setString(1, user.getName());
                        ps.setString(2, user.getEmail());
                        ps.setLong(3, user.getId());
                        return ps;
                    }
            );

            if (rowsAffected != 1) {
                final String errorMessage = MessageFormat.format("Error while updating user. [user={0}, rowsAffected={1}]", user, rowsAffected);
                throw new DemoDataException(errorMessage);
            }

        } catch (Exception e) {
            final String errorMessage = MessageFormat.format("Error on update user [user={0}, Error=[{1}]", user, e.getMessage());
            throw new DemoException(errorMessage, e);
        }
    }


}
