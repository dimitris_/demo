package com.dz.demo.demoapp.repository;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.model.domain.IP;

import java.util.List;

/**
 * The interface IP repository.
 */
public interface IPRepository {

    /**
     * Create ip object.
     *
     * @param ip the ip
     * @throws DemoException the demo exception
     */
    void createIP(IP ip) throws DemoException;

    /**
     * Gets IPs.
     *
     * @return the IPs
     * @throws DemoException the demo exception
     */
    List<IP> getIPs() throws DemoException;

    /**
     * Update ip.
     *
     * @param ip the ip
     * @throws DemoDataException the demo data exception
     * @throws DemoException     the demo exception
     */
    void updateIP(IP ip) throws DemoDataException, DemoException;

    /**
     * Delete ip.
     *
     * @param id the id
     * @throws DemoDataException the demo data exception
     * @throws DemoException     the demo exception
     */
    void deleteIP(long id) throws DemoDataException, DemoException;
}
