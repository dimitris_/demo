package com.dz.demo.demoapp.repository;

import com.dz.demo.demoapp.exception.DemoDataException;
import com.dz.demo.demoapp.exception.DemoException;
import com.dz.demo.demoapp.model.domain.User;

import java.util.List;

/**
 * The interface User repository.
 */
public interface UserRepository {

    /**
     * Create the user.
     *
     * @param user the user
     * @throws DemoException the demo exception
     */
    void create(final User user) throws DemoException;

    /**
     * Delete the user.
     *
     * @param id the id
     * @throws DemoException     the demo exception
     * @throws DemoDataException the demo data exception
     */
    void delete(Long id) throws DemoException, DemoDataException;

    /**
     * Get users.
     *
     * @return the Users
     * @throws DemoException the demo exception
     */
    List<User> getUsers() throws DemoException;

    /**
     * Update the user.
     *
     * @param user the user
     * @throws DemoException     the demo exception
     * @throws DemoDataException the demo data exception
     */
    void update(User user) throws DemoException, DemoDataException;
}
