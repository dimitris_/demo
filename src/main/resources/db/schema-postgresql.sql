--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: IP_HOSTNAME_LOOKUP; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."IP_HOSTNAME_LOOKUP" (
    "IP" inet NOT NULL,
    "HOSTNAME" text NOT NULL,
    "ID" integer NOT NULL
);


ALTER TABLE public."IP_HOSTNAME_LOOKUP" OWNER TO postgres;

--
-- Name: IP_HOSTNAME_LOOKUP_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."IP_HOSTNAME_LOOKUP_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."IP_HOSTNAME_LOOKUP_ID_seq" OWNER TO postgres;

--
-- Name: IP_HOSTNAME_LOOKUP_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."IP_HOSTNAME_LOOKUP_ID_seq" OWNED BY public."IP_HOSTNAME_LOOKUP"."ID";


--
-- Name: ROLE; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ROLE" (
    "NAME" text NOT NULL,
    "ID" integer NOT NULL
);


ALTER TABLE public."ROLE" OWNER TO postgres;

--
-- Name: ROLE_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."ROLE_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ROLE_ID_seq" OWNER TO postgres;

--
-- Name: ROLE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."ROLE_ID_seq" OWNED BY public."ROLE"."ID";


--
-- Name: ROLE_USER_MAP; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ROLE_USER_MAP" (
    "USER_ID" integer NOT NULL,
    "ROLE_ID" integer NOT NULL,
    "ID" integer NOT NULL
);


ALTER TABLE public."ROLE_USER_MAP" OWNER TO postgres;

--
-- Name: ROLE_USER_MAP_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."ROLE_USER_MAP_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ROLE_USER_MAP_ID_seq" OWNER TO postgres;

--
-- Name: ROLE_USER_MAP_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."ROLE_USER_MAP_ID_seq" OWNED BY public."ROLE_USER_MAP"."ID";


--
-- Name: USERS; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."USERS" (
    "ID" integer NOT NULL,
    "NAME" text NOT NULL,
    "PASSWORD" text NOT NULL,
    "EMAIL" text NOT NULL
);


ALTER TABLE public."USERS" OWNER TO postgres;

--
-- Name: USERS_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."USERS_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."USERS_ID_seq" OWNER TO postgres;

--
-- Name: USERS_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."USERS_ID_seq" OWNED BY public."USERS"."ID";


--
-- Name: IP_HOSTNAME_LOOKUP ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."IP_HOSTNAME_LOOKUP" ALTER COLUMN "ID" SET DEFAULT nextval('public."IP_HOSTNAME_LOOKUP_ID_seq"'::regclass);


--
-- Name: ROLE ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ROLE" ALTER COLUMN "ID" SET DEFAULT nextval('public."ROLE_ID_seq"'::regclass);


--
-- Name: ROLE_USER_MAP ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ROLE_USER_MAP" ALTER COLUMN "ID" SET DEFAULT nextval('public."ROLE_USER_MAP_ID_seq"'::regclass);


--
-- Name: USERS ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."USERS" ALTER COLUMN "ID" SET DEFAULT nextval('public."USERS_ID_seq"'::regclass);


--
-- Name: IP_HOSTNAME_LOOKUP_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."IP_HOSTNAME_LOOKUP_ID_seq"', 7, true);


--
-- Name: ROLE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."ROLE_ID_seq"', 3, true);


--
-- Name: ROLE_USER_MAP_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."ROLE_USER_MAP_ID_seq"', 22, true);


--
-- Name: USERS_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."USERS_ID_seq"', 21, true);


--
-- Name: IP_HOSTNAME_LOOKUP IP_HOSTNAME_LOOKUP_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."IP_HOSTNAME_LOOKUP"
    ADD CONSTRAINT "IP_HOSTNAME_LOOKUP_pkey" PRIMARY KEY ("ID");


--
-- Name: ROLE_USER_MAP ROLE_USER_MAP_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ROLE_USER_MAP"
    ADD CONSTRAINT "ROLE_USER_MAP_pkey" PRIMARY KEY ("ID");


--
-- Name: ROLE ROLE_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ROLE"
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY ("ID");


--
-- Name: USERS USERS_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."USERS"
    ADD CONSTRAINT "USERS_pkey" PRIMARY KEY ("ID");


--
-- Name: ROLE_USER_MAP ROLE_USER_MAP_ROLE_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ROLE_USER_MAP"
    ADD CONSTRAINT "ROLE_USER_MAP_ROLE_fkey" FOREIGN KEY ("ROLE_ID") REFERENCES public."ROLE"("ID");


--
-- Name: ROLE_USER_MAP ROLE_USER_MAP_USER_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ROLE_USER_MAP"
    ADD CONSTRAINT "ROLE_USER_MAP_USER_fkey" FOREIGN KEY ("USER_ID") REFERENCES public."USERS"("ID");


--
-- PostgreSQL database dump complete
--
